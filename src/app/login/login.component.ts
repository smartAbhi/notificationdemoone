import { Component, OnInit } from '@angular/core';
import { AngularFireDatabase } from '@angular/fire/compat/database';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import NotificationModel from '../models/Notification';
import { NotificationService } from '../services/notification.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  oNotification: NotificationModel = new NotificationModel();

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private notificationService: NotificationService
  ) { }

  loginForm = this.formBuilder.group({
    username: ['', Validators.required],
    password: ['', Validators.required]
  });

  ngOnInit(): void {

  }
  onSubmit() {
    let oLogin = this.loginForm.value;
    this.oNotification.key = oLogin.username;
    this.oNotification.action = "LoggedIn";
    this.oNotification.message = "User was logged In.";

    this.notificationService.createObject(oLogin.username, this.oNotification).then(() => {
      sessionStorage.setItem('fireBaseUserKey', this.oNotification.key);
      this.router.navigateByUrl('/dashboard');
    });

  }
}
