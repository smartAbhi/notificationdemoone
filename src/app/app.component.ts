import { Component } from '@angular/core';
import { AngularFireDatabase } from '@angular/fire/compat/database';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import NotificationModel from './models/Notification';
import { NotificationService } from './services/notification.service';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  courses: any = [];
  oNotification: NotificationModel = new NotificationModel();

  constructor(private notificationService: NotificationService) {
  }

  ngOnInit() {
    //this.retrieveNotifications();
  }

  onSubmit() {
    this.notificationService.createObject(this.oNotification.key, this.oNotification).then(() => {
      // alert('Created new item successfully!');      
    });
  }

  deleteAll() {
    this.notificationService.deleteAll().then(() => {
      alert('All records deleted!');
    });
  }

  retrieveNotifications() {
    //this.courses = this.notificationService.getAll().valueChanges();

    this.notificationService.getAll()
      .snapshotChanges()
      .pipe(
        map(changes => changes.map(c => ({ key: c.payload.key, ...c.payload.val() })))
      ).subscribe(data => {
        console.log(data);
        this.courses = data;
      });
  }

}
