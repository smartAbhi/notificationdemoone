import { Component, OnInit } from '@angular/core';
import { AngularFireDatabase } from '@angular/fire/compat/database';
import { ToastrService } from 'ngx-toastr';
import { Action } from 'rxjs/internal/scheduler/Action';
import { map } from 'rxjs/operators';
import { NotificationService } from '../services/notification.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  userKey: string = "";
  items: any = [];
  itemChanges: any = {};

  constructor(private _db: AngularFireDatabase
    , private notificationService: NotificationService
    ,private toastr: ToastrService) {

  }

  ngOnInit(): void {
    this.userKey = sessionStorage.getItem('fireBaseUserKey') || "";

    let firebaseDbPath = '/items/' + this.userKey;
    this._db.object(firebaseDbPath)
      .snapshotChanges()
      .pipe(map(a => {
        const data = a.payload.val();
        const id = a.payload.key;
        return { id, data }
      })
      )
      .subscribe(data => {
        this.itemChanges = data;
        this.toastr.success(this.itemChanges.data.message)
        // console.log('userKey', data);
      });

    //this.getAllItems();
  }

  getAllItems() {
    this.notificationService.getAll()
      .snapshotChanges()
      .pipe(
        map(changes => changes.map(c => ({ key: c.payload.key, ...c.payload.val() })))
      ).subscribe(data => {
        console.log('items', this.items);
        this.items = data;
      });
  }

  NotifyUser(oItem: any) {
    let firebaseDbPath = '/items/' + this.userKey;
    this.notificationService.update(oItem.key, oItem).then(() => {
      alert('Notified..');
    });
  }

}
