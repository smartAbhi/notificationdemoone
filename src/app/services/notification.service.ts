import { Injectable } from '@angular/core';
import { AngularFireDatabase, AngularFireList } from '@angular/fire/compat/database';
import NotificationModel from '../models/Notification';

@Injectable({
  providedIn: 'root'
})
export class NotificationService {

  //Refers to DB path root object
  private dbPath = '/items';

  //AngularFireList service helps us to synchronize data as lists
  notificationsRef: AngularFireList<NotificationModel>;

  //AngularFireDatabase service injected in ctor allows us to work with the Realtime Database.
  constructor(private db: AngularFireDatabase) {
    this.notificationsRef = db.list(this.dbPath);
  }

  getAll(): AngularFireList<NotificationModel> {
    return this.notificationsRef;
  }
  
  //push will generate a new random key and insert the data.
  create(notification: NotificationModel): any {
    return this.notificationsRef.push(notification);
  }

  //set the specific record with whatever data you passed in.
  createObject(key: string, value: any): Promise<void> {
    return this.notificationsRef.set(key,value);
  }

  update(key: string, value: any): Promise<void> {
    return this.notificationsRef.update(key, value);
  }

  delete(key: string): Promise<void> {
    return this.notificationsRef.remove(key);
  }

  deleteAll(): Promise<void> {
    return this.notificationsRef.remove();
  }

}
