export default class NotificationModel {
  key: string = "";
  action: string = "";
  message: string = "";
}